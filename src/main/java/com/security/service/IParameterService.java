package com.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.security.entity.Parameter;
import com.security.util.RespBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
public interface IParameterService extends IService<Parameter> {

    /**
     * 添加产品参数
     * @param parameterName
     * @return
     */
    RespBean addParameter(String parameterName);

    /**
     * 获取参数列表
     * @return
     */
    RespBean getParameters();

    /**
     * 根据产品名称查询对应的参数
     * @param categoryName
     * @return
     */
    RespBean getParameterByCategoryName(String categoryName);
}
