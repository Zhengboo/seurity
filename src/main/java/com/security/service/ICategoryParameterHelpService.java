package com.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.security.entity.CategoryParameterHelp;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
public interface ICategoryParameterHelpService extends IService<CategoryParameterHelp> {

}
