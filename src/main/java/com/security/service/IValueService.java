package com.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.security.entity.Value;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
public interface IValueService extends IService<Value> {

}
