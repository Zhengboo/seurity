package com.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.security.entity.Admin;
import com.security.util.RespBean;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
public interface IAdminService extends IService<Admin> {

    /**
     * 登录
     * @param admin
     * @param response
     * @return
     */
    boolean login(Admin admin, HttpServletResponse response);

    /**
     * 管理员登出功能
     * @param token
     * @return
     */
    RespBean logout(String token);
}
