package com.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.security.entity.News;
import com.security.util.RespBean;

import java.util.Date;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
public interface INewsService extends IService<News> {

    /**
     * 获取新闻内容
     * @return
     */
    RespBean getNews();

    /**
     * 添加新闻内容
     * @param newsTitle
     * @param newsContent
     * @param newsDate
     * @param newsImage
     * @return
     */
    RespBean addNews(String newsTitle, String newsContent, Date newsDate, String newsImage);
}
