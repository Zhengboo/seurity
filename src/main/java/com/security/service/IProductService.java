package com.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.security.entity.Product;
import com.security.util.RespBean;
import com.security.vo.ProductParameterVo;
import com.security.vo.ProductVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
public interface IProductService extends IService<Product> {

    /**
     * 获取所有产品
     * @return
     */
    RespBean getProducts();

    /**
     * 添加产品
     * @param productParameterVo
     * @return
     */
    RespBean addProduct(ProductParameterVo productParameterVo);

    /**
     * 通过产品分类id查询其对应的的产品
     * @param categoryId
     * @return
     */
    RespBean getProductByCategoryId(int categoryId);

}
