package com.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.security.entity.CategoryParameterHelp;
import com.security.mapper.CategoryParameterHelpMapper;
import com.security.service.ICategoryParameterHelpService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
@Service
public class CategoryParameterHelpServiceImpl extends ServiceImpl<CategoryParameterHelpMapper, CategoryParameterHelp> implements ICategoryParameterHelpService {

}
