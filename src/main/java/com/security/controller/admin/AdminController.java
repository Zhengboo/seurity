package com.security.controller.admin;
import com.security.entity.Admin;
import com.security.service.IAdminService;
import com.security.util.RespBean;
import com.security.util.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
@RestController
@RequestMapping("/admin")
public class AdminController {


    @Autowired
    private IAdminService adminService;

    /**
     * 登录
     * @param admin
     * @param response
     * @return
     */
    @RequestMapping("/login")
    @ResponseBody
    public RespBean login(Admin admin, HttpServletResponse response){
        if (adminService.login(admin,response)){
            return RespBean.success();
        }else{
            return RespBean.error(RespBeanEnum.ERROR);
        }
    }

    /**
     * 管理员登出功能
     * @param token
     * @return
     */
    @RequestMapping("/logout")
    public RespBean logout(String token){
        return adminService.logout(token);
    }
}
