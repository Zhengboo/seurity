package com.security.controller.admin;
import com.security.util.QiniuCloudUtil;
import com.security.util.RespBean;
import com.security.util.RespBeanEnum;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

/**
 * @Description:上传图片到七牛云
 * @Author:啵唧啵唧~~
 * @Date:2022/3/6
 */
@RestController
@RequestMapping("/qiniu")
public class QiNiuCloudContorller {

    @ResponseBody
    @RequestMapping(value="/uploadImg", method= RequestMethod.POST)
    public RespBean uploadImg(MultipartFile image, HttpServletRequest request) {
        RespBean respBean = new RespBean();
        //判断图片文件是否为空
        if (image.isEmpty()) {
            respBean.setCode(RespBeanEnum.EMPTY_ERROR.getCode());
            respBean.setMessage(RespBeanEnum.EMPTY_ERROR.getMessage());
            return respBean;
        }

        try {
            byte[] bytes = image.getBytes();
            String imageName = UUID.randomUUID().toString();
            try {
                //使用base64方式上传到七牛云
                String url = QiniuCloudUtil.put64image(bytes, imageName);
                respBean.setCode(RespBeanEnum.SUCCESS.getCode());
                respBean.setMessage(RespBeanEnum.SUCCESS.getMessage());
                respBean.setMessage(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            respBean.setCode(RespBeanEnum.QINIUEXCEPTION_ERROR.getCode());
            respBean.setMessage(RespBeanEnum.QINIUEXCEPTION_ERROR.getMessage());
        } finally {
            return respBean;
        }
    }
}
