package com.security.controller.admin;

import com.security.service.IProductService;
import com.security.util.RespBean;
import com.security.vo.ProductParameterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private IProductService productService;

    /**
     *获取所有产品
     * @return
     */
    @RequestMapping("/getProducts")
    public RespBean getProducts(){
        return productService.getProducts();
    }

    /**
     * 添加产品信息
     * @paramProductParameterVo
     * @return
     */
    @RequestMapping("/addProduct")
    public RespBean addProduct(ProductParameterVo productParameterVo){
        return productService.addProduct(productParameterVo);
    }

    /**
     * 通过产品分类id查询其对应的的产品
     * @param categoryId
     * @return
     */
    @RequestMapping("/getProductByCategoryId")
    public RespBean getProductByCategoryId(int categoryId){
        return productService.getProductByCategoryId(categoryId);
    }
}
