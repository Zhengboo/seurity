package com.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.security.entity.Admin;
import com.security.entity.Images;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zb
 * @since 2022-03-04
 */
@Repository
@Mapper
public interface ImagesMapper extends BaseMapper<Images> {

}
