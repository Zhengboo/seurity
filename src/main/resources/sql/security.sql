/*
 Navicat Premium Data Transfer

 Source Server         : 123
 Source Server Type    : MySQL
 Source Server Version : 50536
 Source Host           : localhost:3306
 Source Schema         : security

 Target Server Type    : MySQL
 Target Server Version : 50536
 File Encoding         : 65001

 Date: 06/03/2022 12:20:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `admin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员名称',
  `admin_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员密码',
  `admin_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员',
  `admin_tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员电话',
  `admin_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员邮箱',
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, '张三', '123456', 'zs', '123456789', '123');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `category_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, '脚手架类');
INSERT INTO `category` VALUES (2, '钢管类');
INSERT INTO `category` VALUES (3, '头盔类');
INSERT INTO `category` VALUES (4, 'categoryName=%E8%9E%BA%E4%B8%9D%E9%92%89%E7%B1%BB&parameters=%E9%95%BF%E5%BA%A6&parameters=%E9%AB%98%E5%BA%A6&parameters=%E6%9D%90%E8%B4%A8');
INSERT INTO `category` VALUES (5, '桌子');

-- ----------------------------
-- Table structure for category_parameter_help
-- ----------------------------
DROP TABLE IF EXISTS `category_parameter_help`;
CREATE TABLE `category_parameter_help`  (
  `category_parameter_help_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '参数和分类维护id',
  `category_parameter_help_category_id` int(10) NULL DEFAULT NULL COMMENT '分类id',
  `category_parameter_help_parameter_id` int(10) NULL DEFAULT NULL COMMENT '参数id',
  PRIMARY KEY (`category_parameter_help_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category_parameter_help
-- ----------------------------
INSERT INTO `category_parameter_help` VALUES (1, 1, 1);
INSERT INTO `category_parameter_help` VALUES (2, 1, 2);
INSERT INTO `category_parameter_help` VALUES (3, 2, 3);
INSERT INTO `category_parameter_help` VALUES (4, 4, 1);
INSERT INTO `category_parameter_help` VALUES (5, 4, 4);
INSERT INTO `category_parameter_help` VALUES (6, 4, 3);
INSERT INTO `category_parameter_help` VALUES (7, 5, 1);
INSERT INTO `category_parameter_help` VALUES (8, 5, 4);
INSERT INTO `category_parameter_help` VALUES (9, 5, 3);

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images`  (
  `images_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '图片id',
  `images_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `images_product_id` int(10) NULL DEFAULT NULL COMMENT '图片对应产品id',
  PRIMARY KEY (`images_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES (1, 'C://123//452', 1);
INSERT INTO `images` VALUES (2, 'C://123//000', 1);
INSERT INTO `images` VALUES (3, 'D://123//452', 1);
INSERT INTO `images` VALUES (4, 'E://123//452', 1);

-- ----------------------------
-- Table structure for information
-- ----------------------------
DROP TABLE IF EXISTS `information`;
CREATE TABLE `information`  (
  `information_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '联系信息id',
  `information_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系地址',
  `information_tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `information_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系邮箱',
  `information_hotline` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系热线',
  `information_fac` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系传真',
  PRIMARY KEY (`information_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of information
-- ----------------------------
INSERT INTO `information` VALUES (1, '陕西省西安市临潼区西安科技大学', '155123456', '123456@163.com', '43960521', '传真001');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `news_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '新闻id',
  `news_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新闻标题',
  `news_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '新闻内容',
  `news_date` date NULL DEFAULT NULL COMMENT '新闻发布日期',
  `news_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新闻图片地址',
  PRIMARY KEY (`news_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (1, '新闻标题1', '今天发布一个智能脚手架', '2022-03-04', 'E://12121');

-- ----------------------------
-- Table structure for parameter
-- ----------------------------
DROP TABLE IF EXISTS `parameter`;
CREATE TABLE `parameter`  (
  `parameter_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '参数id',
  `parameter_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数名',
  PRIMARY KEY (`parameter_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of parameter
-- ----------------------------
INSERT INTO `parameter` VALUES (1, '长度');
INSERT INTO `parameter` VALUES (2, '宽度');
INSERT INTO `parameter` VALUES (3, '材质');
INSERT INTO `parameter` VALUES (4, '高度');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `product_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '产品id',
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名',
  `product_category_id` int(10) NULL DEFAULT NULL COMMENT '产品分类id',
  `product_describe` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '产品描述',
  PRIMARY KEY (`product_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '脚手架1', 1, '这是一个智能的脚手架001');

-- ----------------------------
-- Table structure for value
-- ----------------------------
DROP TABLE IF EXISTS `value`;
CREATE TABLE `value`  (
  `value_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '参数值id',
  `value_product_id` int(10) NULL DEFAULT NULL COMMENT '产品id',
  `value_parameter_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名',
  `value_parameter_id` int(10) NULL DEFAULT NULL COMMENT '参数id',
  `value_parameter_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数值',
  PRIMARY KEY (`value_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of value
-- ----------------------------
INSERT INTO `value` VALUES (1, 1, '材质', 3, '金属');
INSERT INTO `value` VALUES (2, 1, '长度', 1, '50');
INSERT INTO `value` VALUES (3, 1, '宽度', 2, '10');

SET FOREIGN_KEY_CHECKS = 1;
